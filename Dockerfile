FROM python:3.10.10-bullseye
RUN mkdir /app
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt && chmod +x start.sh
CMD  /bin/bash start.sh