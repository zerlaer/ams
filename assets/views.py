from rest_framework import viewsets

from assets.models import Computer, Printer, Server, NetworkDevice, Manufacturer
from assets.serializers import ComputerSerializer, PrinterSerializer, ServerSerializer, NetworkDeviceSerializer, \
    ManufacturerSerializer


class ComputerViewSet(viewsets.ModelViewSet):
    """
    Computer:电脑
    """
    queryset = Computer.objects.all()
    serializer_class = ComputerSerializer


class PrinterViewSet(viewsets.ModelViewSet):
    """
    Printer:打印机
    """
    queryset = Printer.objects.all()
    serializer_class = PrinterSerializer


class ServerViewSet(viewsets.ModelViewSet):
    """
    Server:服务器
    """
    queryset = Server.objects.all()
    serializer_class = ServerSerializer


class NetworkDeviceViewSet(viewsets.ModelViewSet):
    """
    NetworkDevice:网络设备
    """
    queryset = NetworkDevice.objects.all()
    serializer_class = NetworkDeviceSerializer


class ManufacturerViewSet(viewsets.ModelViewSet):
    """
    Manufacturer:供应商
    """
    queryset = Manufacturer.objects.all()
    serializer_class = ManufacturerSerializer
