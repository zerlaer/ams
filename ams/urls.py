from django.conf.urls import include
from django.contrib import admin
from django.urls import path

from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from assets import views
from users.views import UserViewSet

router = routers.DefaultRouter()
router.register(r'computer', views.ComputerViewSet)
router.register(r'printer', views.PrinterViewSet)
router.register(r'server', views.ServerViewSet)
router.register(r'networkdevice', views.NetworkDeviceViewSet)
router.register(r'manufacturer', views.ManufacturerViewSet)
router.register(r'user', UserViewSet)

urlpatterns = [
    path('', admin.site.urls),
    path('ams/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
    path('token/', TokenObtainPairView.as_view(), name='token'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token refresh'),
    path('schema/', SpectacularAPIView.as_view(), name='schema'),
    path('api/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('doc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),

]
